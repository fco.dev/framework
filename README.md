## 🏃 How to build and run locally

```
yarn ios
```

```
npm run ios
```

- Build/Run on Android 🤖

```
yarn android
```

```
npm run android
```

## 💄 How to start tailwind watching

```
yarn dev:tailwind
```

## ☁️ Build in the cloud

- [Building with EAS](https://docs.expo.dev/eas/)

## 📝 Notes

- [Realm JS Documentation](https://docs.mongodb.com/realm/sdk/react-native/)
- [@realm/react Readme](https://github.com/realm/realm-js/tree/master/packages/realm-react#readme)
- [Tailwind](https://github.com/vadimdemedes/tailwind-rn)
