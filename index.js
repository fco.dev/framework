import "expo-dev-client";
import "react-native-get-random-values";
import React from "react";
import { useColorScheme } from "react-native";
import { registerRootComponent } from "expo";
import { RealmContext } from "./app/services/realm";

import { TailwindProvider } from "tailwind-rn";

import utilities from "./tailwind.json";
import AppNavigation from "./app/navigation/AppNavigation";

const App = () => {
  const { RealmProvider } = RealmContext;
  return (
    <TailwindProvider utilities={utilities} colorScheme={useColorScheme()}>
      <RealmProvider>
        <AppNavigation />
      </RealmProvider>
    </TailwindProvider>
  );
};

registerRootComponent(App);
