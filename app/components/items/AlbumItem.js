import { memo } from "react";
import { View, Text } from "react-native";
import { useTailwind } from "tailwind-rn";

const AlbumItem = ({ album }) => {
  const tw = useTailwind();

  return (
    <View style={tw("flex-row rounded-lg bg-white mb-2 p-3 items-center mx-4")}>
      <Text style={tw("flex-1 mr-2")}>{album?.title}</Text>
    </View>
  );
};

export default memo(AlbumItem);
