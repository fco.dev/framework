import { memo } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { useTailwind } from "tailwind-rn";
import { Ionicons } from "@expo/vector-icons";

const PostItem = ({ post, onPress }) => {
  const tw = useTailwind();

  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={tw("flex-row rounded-lg bg-white mb-2 p-3 items-center mx-4")}
      >
        <Text style={tw("flex-1 mr-2")}>{post?.title}</Text>
        <Ionicons
          name="md-chevron-forward"
          size={24}
          color={tw("text-black/40")?.color}
        />
      </View>
    </TouchableOpacity>
  );
};

export default memo(PostItem);
