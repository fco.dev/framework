import { memo } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { useTailwind } from "tailwind-rn";
import { Ionicons } from "@expo/vector-icons";
import cn from "classnames";

const TaskItem = ({ post, onPress }) => {
  const tw = useTailwind();
  const status = post?.completed ? "✓" : "X";

  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={tw("flex-row rounded-lg bg-white mb-2 p-3 items-center mx-4")}
      >
        <Text
          style={tw(
            cn("p-1 mr-1 text-base", {
              "bg-teal-300": post?.completed,
              "bg-rose-300": !post?.completed,
            })
          )}
        >
          {status}
        </Text>
        <Text style={tw("flex-1 mr-2")}>{post?.title}</Text>

        <Ionicons
          name="md-chevron-forward"
          size={24}
          color={tw("text-black/40")?.color}
        />
      </View>
    </TouchableOpacity>
  );
};

export default memo(TaskItem);
