import { View } from "react-native";
import { Placeholder, PlaceholderLine, Fade } from "rn-placeholder";
import { useTailwind } from "tailwind-rn";
import { Ionicons } from "@expo/vector-icons";

const Loading = () => {
  return (
    <View>
      <Item />
      <Item />
      <Item />
      <Item />
    </View>
  );
};

const Item = () => {
  const tw = useTailwind();
  return (
    <Placeholder Animation={Fade}>
      <View style={tw("flex-row rounded-lg bg-white mb-2 p-3 items-center")}>
        <View style={tw("flex-1")}>
          <PlaceholderLine width={90} height={12} noMargin style={tw("")} />
          <PlaceholderLine
            width={70}
            height={12}
            noMargin
            style={tw("mt-1.5")}
          />
        </View>
        <Ionicons
          name="md-chevron-forward"
          size={24}
          color={tw("text-black/40")?.color}
        />
      </View>
    </Placeholder>
  );
};
export default Loading;
