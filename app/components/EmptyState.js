import { View, Text, TouchableOpacity } from "react-native";
import { useTailwind } from "tailwind-rn";
import { Feather } from "@expo/vector-icons";

const EmptyState = ({ onUpdate }) => {
  const tw = useTailwind();
  return (
    <View style={tw("items-center")}>
      <TouchableOpacity style={tw("items-center py-2")} onPress={onUpdate}>
        <Feather name="refresh-cw" size={24} color="black" />
        <Text style={tw("mt-2")}>Toque para atualizar</Text>
      </TouchableOpacity>
    </View>
  );
};
export default EmptyState;
