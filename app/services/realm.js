import { createRealmContext } from "@realm/react";
import { PostSchema } from "../schemas/PostSchema";
import { AlbumSchema } from "../schemas/AlbumSchema";
import { TaskSchema } from "../schemas/TaskSchema";

export const RealmContext = createRealmContext({
  schema: [PostSchema, AlbumSchema, TaskSchema],
});
