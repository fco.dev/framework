import axios from "axios";

const api = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/",
  timeout: 1000 * 5,
});

export default api;
