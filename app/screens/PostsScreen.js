import React, { useState, useCallback } from "react";
import { View, Text, FlatList, RefreshControl } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { useTailwind } from "tailwind-rn";

import PostItem from "../components/items/PostItem";
import PostsLoading from "../components/Loading";
import PostEmptyState from "../components/EmptyState";

import api from "../services/api";
import { RealmContext } from "../services/realm";
import { PostSchema } from "../schemas/PostSchema";

const { useQuery, useRealm } = RealmContext;

function PostsScreen({ navigation }) {
  const tw = useTailwind();
  const [loading, setLoading] = useState(false);
  const realm = useRealm();
  const postsFromRealm = useQuery(PostSchema);

  const savePosts = useCallback(
    async (posts) => {
      if (!!!posts?.length) return;

      realm.write(() => {
        for (const post of posts) {
          realm.create("Post", post, "modified");
        }
      });
    },
    [realm]
  );

  const getPosts = useCallback(async () => {
    try {
      const { data } = await api.get("posts");
      return data;
    } catch (error) {
      console.error(error);
    }
  }, [api]);

  async function handleData() {
    setLoading(true);
    const posts = await getPosts();
    await savePosts(posts);
    setLoading(false);
  }

  useFocusEffect(
    useCallback(() => {
      handleData();
    }, [])
  );

  const goToPostDetails = useCallback(
    (id) =>
      navigation.navigate("PostDetails", {
        id,
      }),
    [navigation]
  );

  return (
    <View style={tw("flex-1 ")}>
      <Text style={tw("text-xl font-bold mb-4 p-4")}>Posts</Text>
      <FlatList
        keyExtractor={(post) => post?.id}
        data={postsFromRealm}
        renderItem={({ item }) => (
          <PostItem post={item} onPress={() => goToPostDetails(item?.id)} />
        )}
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={handleData} />
        }
        ListEmptyComponent={() =>
          loading ? (
            <PostsLoading />
          ) : (
            <PostEmptyState onUpdate={() => handleData()} />
          )
        }
      />
    </View>
  );
}

export default PostsScreen;
