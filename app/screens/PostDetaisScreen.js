import React from "react";
import { View, Text } from "react-native";
import { useTailwind } from "tailwind-rn";

import { RealmContext } from "../services/realm";

const { useRealm } = RealmContext;

function PostDetaisScreen({ route }) {
  const tw = useTailwind();
  const realm = useRealm();

  const id = route?.params?.id;
  const post = realm.objectForPrimaryKey("Post", id);

  return (
    <View style={tw("flex-1 p-4")}>
      <Text style={tw("text-xl font-bold")}>{post?.title}</Text>
      <Text style={tw("text-xl font-normal mt-4")}>{post?.body}</Text>
    </View>
  );
}

export default PostDetaisScreen;
