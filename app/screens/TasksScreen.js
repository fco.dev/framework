import React, { useState, useCallback, useMemo } from "react";
import { View, Text, FlatList, RefreshControl } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { useTailwind } from "tailwind-rn";

import TaskItem from "../components/items/TaskItem";
import Loading from "../components/Loading";
import EmptyState from "../components/EmptyState";

import api from "../services/api";
import { RealmContext } from "../services/realm";
import { TaskSchema } from "../schemas/TaskSchema";

const { useQuery, useRealm } = RealmContext;

function TasksScreen({ navigation }) {
  const tw = useTailwind();
  const [loading, setLoading] = useState(false);
  const realm = useRealm();
  const tasksFromRealm = useQuery(TaskSchema);

  const tasks = useMemo(
    () => tasksFromRealm.sorted(["completed", ["updateAt", true]]),
    [tasksFromRealm]
  );

  const saveTasks = useCallback(
    async (tasks) => {
      if (!!!tasks?.length) return;

      realm.write(() => {
        for (const task of tasks) {
          const existingTask = realm.objectForPrimaryKey("Task", task.id);
          if (!existingTask?.updateAt) {
            realm.create("Task", { ...task, updateAt: new Date() }, "modified");
          }
        }
      });
    },
    [realm]
  );

  const getTasks = useCallback(async () => {
    try {
      const { data } = await api.get("todos");
      return data;
    } catch (error) {
      console.error(error);
    }
  }, [api]);

  async function handleData() {
    setLoading(true);
    const tasks = await getTasks();
    await saveTasks(tasks);
    setLoading(false);
  }

  useFocusEffect(
    useCallback(() => {
      handleData();
    }, [])
  );

  const goToTaskDetails = useCallback(
    (id) =>
      navigation.navigate("TaskDetails", {
        id,
      }),
    [navigation]
  );

  return (
    <View style={tw("flex-1 ")}>
      <Text style={tw("text-xl font-bold mb-4 p-4")}>To Do</Text>
      <FlatList
        keyExtractor={(post) => post?.id}
        data={tasks}
        renderItem={({ item }) => (
          <TaskItem post={item} onPress={() => goToTaskDetails(item?.id)} />
        )}
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={handleData} />
        }
        ListEmptyComponent={() =>
          loading ? <Loading /> : <EmptyState onUpdate={() => handleData()} />
        }
      />
    </View>
  );
}

export default TasksScreen;
