import React, { useState, useCallback } from "react";
import { View, Text, FlatList, RefreshControl } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { useTailwind } from "tailwind-rn";

import AlbumItem from "../components/items/AlbumItem";
import AlbumsLoading from "../components/Loading";
import AlbumsEmptyState from "../components/EmptyState";

import api from "../services/api";
import { RealmContext } from "../services/realm";
import { AlbumSchema } from "../schemas/AlbumSchema";

const { useQuery, useRealm } = RealmContext;

function AlbumScreen() {
  const tw = useTailwind();
  const [loading, setLoading] = useState(false);
  const realm = useRealm();
  const albumsFromRealm = useQuery(AlbumSchema);

  const saveAlbums = useCallback(
    async (albums) => {
      if (!!!albums?.length) return;

      realm.write(() => {
        for (const album of albums) {
          realm.create("Album", album, "modified");
        }
      });
    },
    [realm]
  );

  const getAlbums = useCallback(async () => {
    try {
      const { data } = await api.get("albums");
      return data;
    } catch (error) {
      console.error(error);
    }
  }, [api]);

  async function handleData() {
    setLoading(true);
    const albums = await getAlbums();
    await saveAlbums(albums);
    setLoading(false);
  }

  useFocusEffect(
    useCallback(() => {
      handleData();
    }, [])
  );

  return (
    <View style={tw("flex-1 ")}>
      <Text style={tw("text-xl font-bold mb-4 p-4")}>Albums</Text>
      <FlatList
        keyExtractor={(album) => album?.id}
        data={albumsFromRealm}
        renderItem={({ item }) => <AlbumItem album={item} />}
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={handleData} />
        }
        ListEmptyComponent={() =>
          loading ? (
            <AlbumsLoading />
          ) : (
            <AlbumsEmptyState onUpdate={() => handleData()} />
          )
        }
      />
    </View>
  );
}

export default AlbumScreen;
