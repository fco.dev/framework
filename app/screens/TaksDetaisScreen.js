import React, { useState, useEffect, useCallback, useRef } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { useTailwind } from "tailwind-rn";
import cn from "classnames";

import { RealmContext } from "../services/realm";

const { useRealm } = RealmContext;

function TaksDetaisScreen({ route }) {
  const tw = useTailwind();
  const realm = useRealm();
  const [currentTask, setCurrentTask] = useState();

  const id = route?.params?.id;
  const task = realm.objectForPrimaryKey("Task", id);

  useEffect(() => {
    task.addListener((task) => {
      setCurrentTask(task.toJSON());
    });
    return () => {
      task.removeAllListeners();
    };
  }, []);

  const status = currentTask?.completed ? "✓" : "X";

  const handleUpdateTask = async () => {
    realm.write(() => {
      task.completed = true;
      task.updateAt = new Date();
    });
  };

  return (
    <View style={tw("flex-1 p-4")}>
      <View style={tw("flex-row items-center")}>
        <Text
          style={tw(
            cn("p-1 mr-1 text-base self-start", {
              "bg-teal-300": currentTask?.completed,
              "bg-rose-300": !currentTask?.completed,
            })
          )}
        >
          {status}
        </Text>
        <Text style={tw("text-xl font-bold")}>{currentTask?.title}</Text>
      </View>
      {!!!currentTask?.completed && (
        <TouchableOpacity
          style={tw("self-end mt-4")}
          onPress={handleUpdateTask}
        >
          <View style={tw("bg-teal-500 p-4 rounded")}>
            <Text>Marcar como feito</Text>
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
}

export default TaksDetaisScreen;
