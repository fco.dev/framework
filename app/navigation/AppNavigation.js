import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { SafeAreaProvider } from "react-native-safe-area-context";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";

import MainStack from "./MainStack";
import PostDetailsScreen from "../screens/PostDetaisScreen";
import TaksDetaisScreen from "../screens/TaksDetaisScreen";

const AppStack = createNativeStackNavigator();

function AppNavigation() {
  return (
    <SafeAreaProvider>
      <StatusBar style="dark" />
      <SafeAreaView edges={["top"]} />
      <NavigationContainer>
        <AppStack.Navigator>
          <AppStack.Screen
            name="Main"
            options={{ headerShown: false }}
            component={MainStack}
          />
          <AppStack.Screen
            name="PostDetails"
            options={{ title: "Detalhes do Post" }}
            component={PostDetailsScreen}
          />
          <AppStack.Screen
            name="TaskDetails"
            options={{ title: "Detalhes da Tarefa" }}
            component={TaksDetaisScreen}
          />
        </AppStack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

export default AppNavigation;
