import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import PostsScreen from "../screens/PostsScreen";
import AlbumsScreen from "../screens/AlbumsScreen";
import TasksScreen from "../screens/TasksScreen";

import Ionicons from "@expo/vector-icons/Ionicons";

const MainStack = createBottomTabNavigator();

const tabIcons = {
  Posts: {
    true: "reader",
    false: "reader-outline",
  },
  Albums: {
    true: "albums",
    false: "albums-outline",
  },
  Tasks: {
    true: "folder-open",
    false: "folder-open-outline",
  },
};

export default function MainNavigator() {
  return (
    <MainStack.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          const tabIcon = tabIcons[route.name][focused];

          return <Ionicons name={tabIcon} size={size} color={color} />;
        },
        tabBarActiveTintColor: "teal",
        tabBarInactiveTintColor: "gray",
      })}
    >
      <MainStack.Screen
        name="Posts"
        options={{ headerShown: false }}
        component={PostsScreen}
      />
      <MainStack.Screen
        name="Albums"
        options={{ headerShown: false }}
        component={AlbumsScreen}
      />
      <MainStack.Screen
        name="Tasks"
        options={{ headerShown: false }}
        component={TasksScreen}
      />
    </MainStack.Navigator>
  );
}
