import { Realm } from "@realm/react";
export class AlbumSchema extends Realm.Object {
  static schema = {
    name: "Album",
    primaryKey: "id",
    properties: {
      id: { type: "int", indexed: true },
      title: "string",
      userId: "int",
    },
  };
}
