import { Realm } from "@realm/react";
export class PostSchema extends Realm.Object {
  static schema = {
    name: "Post",
    primaryKey: "id",
    properties: {
      id: { type: "int", indexed: true },
      title: "string",
      body: "string",
      userId: "int",
    },
  };
}
