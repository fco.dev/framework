import { Realm } from "@realm/react";
export class TaskSchema extends Realm.Object {
  static schema = {
    name: "Task",
    primaryKey: "id",
    properties: {
      id: { type: "int", indexed: true },
      title: "string",
      userId: "int",
      completed: "bool",
      updateAt: "date?",
    },
  };
}
